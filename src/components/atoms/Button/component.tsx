import { css } from '@emotion/core';
import styled from '@emotion/styled';
import React, { FC } from 'react';

import { colors } from '../../../core';
import { Loading } from '../Loading';
import { Props } from './props';

const ButtonBase: FC<Props> = ({ children, loading, ...rest }: Props) => (
  <button {...rest}>{loading ? <Loading /> : children}</button>
);

export const Button = styled(ButtonBase)<Props>`
  ${({ appearance, rounded, disabled }) =>
    css`
      display: inline-block;
      border-radius: ${rounded ? 100 : 4}px;
      border: none;
      cursor: pointer;
      font-size: 13px;
      font-family: inherit;
      font-weight: 600;
      padding: 8px 15px;
      transition: border-color 0.3s, color 0.3s, background 0.3s;
      ${disabled
        ? css`
            color: ${colors.Text.Grey};
            border: 1px solid ${colors.Text.Grey};
          `
        : css`
        ${
          appearance === 'primary' &&
          css`
            background: ${colors.Brand.Primary};
            color: ${colors.Text.Light};
            border: 1px solid ${colors.Brand.Primary};
            &:hover {
              border-color: ${colors.Brand.PrimaryDark};
              background: ${colors.Brand.PrimaryDark};
            }
          `
        }
        ${
          appearance === 'default' &&
          css`
            border: 1px solid ${colors.Text.Grey};
            background: ${colors.Background.White};
            color: ${colors.Text.Black};
            &:hover {
              border-color: ${colors.Brand.Primary};
              color: ${colors.Brand.Primary};
            }
          `
        }
        ${
          appearance === 'dashed' &&
          css`
            border: 1px dashed ${colors.Text.Grey};
            background: ${colors.Background.Transparent};
            color: ${colors.Text.Dark};
            &:hover {
              border-color: ${colors.Brand.Primary};
              color: ${colors.Brand.Primary};
            }
          `
        }
        ${
          appearance === 'text' &&
          css`
            background: ${colors.Background.Transparent};
            color: ${colors.Text.Black};
          `
        }
        ${
          appearance === 'link' &&
          css`
            background: ${colors.Background.Transparent};
            color: ${colors.Brand.Primary};
            &:hover {
              text-decoration: underline;
            }
            &:active {
              color: ${colors.Brand.PrimaryDark};
            }
          `
        }
      `}
    `}
`;

Button.defaultProps = {
  appearance: 'default',
};
