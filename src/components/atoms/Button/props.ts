import { ButtonHTMLAttributes } from 'react';

export type Props = ButtonHTMLAttributes<HTMLButtonElement> & {
  readonly appearance?: 'default' | 'primary' | 'dashed' | 'text' | 'link';
  readonly loading?: boolean;
  readonly rounded?: boolean;
};
