import React, { FC } from 'react';

import { Props } from './props';

export const Loading: FC<Props> = (props) => {
  return <span {...props}>Loading...</span>;
};
