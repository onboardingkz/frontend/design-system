import { HTMLAttributes } from 'react';

export type Props = HTMLAttributes<HTMLDivElement> & {
  readonly borderSize?: number;
  readonly borderRadius?: number;
  readonly disableShadow?: boolean;
};
