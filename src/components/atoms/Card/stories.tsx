import { Story } from '@storybook/react';
import React from 'react';

import { StorybookAppearanceTypes } from '../../../core/storybook';
import { Card } from './component';
import { CardProps } from './index';

export default {
  title: 'atoms/Card',
  component: Card,
  argTypes: {
    borderSize: {
      description: 'Size of border (px)',
      table: {
        category: StorybookAppearanceTypes.Border,
        defaultValue: { summary: 1 },
      },
      control: 'number',
    },
    borderRadius: {
      description: 'Radius of border (px)',
      table: {
        category: StorybookAppearanceTypes.Border,
        defaultValue: { summary: 4 },
      },
      control: 'number',
    },
    disableShadow: {
      description: 'Whether component should have shadow or not',
      table: {
        defaultValue: { summary: false },
      },
      control: 'boolean',
    },
  },
};

export const Appearance: Story<CardProps> = (args) => {
  return (
    <Card className={'py-3 px-4'} {...args}>
      <p>Lorem ipsum dolor sit amet</p>
    </Card>
  );
};

// eslint-disable-next-line functional/immutable-data
Appearance.args = {
  disableShadow: false,
  borderRadius: 4,
  borderSize: 1,
};
