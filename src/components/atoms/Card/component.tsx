import { css } from '@emotion/core';
import styled from '@emotion/styled';

import { colors } from '../../../core';
import { Props } from './props';

export const Card = styled.div<Props>`
  ${({ disableShadow, borderSize, borderRadius }) =>
    css`
      border-radius: ${borderRadius}px;
      box-shadow: ${disableShadow ? `none` : `0 7px 13px -7px rgb(0, 0, 0, 0.2)`};
      border: ${borderSize}px solid ${colors.Background.Grey};
      transition: box-shadow 0.2s;
    `}
`;

Card.defaultProps = {
  borderSize: 1,
};
